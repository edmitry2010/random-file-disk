########## smartctl -a /dev/sda ##########
smartctl 7.4 2023-08-01 r5530 [x86_64-linux-6.5.7-200.fc38.x86_64] (local build)
Copyright (C) 2002-23, Bruce Allen, Christian Franke, www.smartmontools.org

=== START OF INFORMATION SECTION ===
Device Model:     ZM-SSD 128GB
Serial Number:    2301VE0R63330046
LU WWN Device Id: 0 000000 000000000
Firmware Version: VE0R6333
User Capacity:    128 035 676 160 bytes [128 GB]
Sector Size:      512 bytes logical/physical
Rotation Rate:    Solid State Device
Form Factor:      2.5 inches
TRIM Command:     Available, deterministic
Device is:        Not in smartctl database 7.3/5528
ATA Version is:   ACS-3, ATA8-ACS T13/1699-D revision 6
SATA Version is:  SATA 3.0, 6.0 Gb/s (current: 1.5 Gb/s)
Local Time is:    Tue Oct 24 09:13:28 2023 +07
SMART support is: Available - device has SMART capability.
SMART support is: Enabled

=== START OF READ SMART DATA SECTION ===
SMART overall-health self-assessment test result: PASSED

General SMART Values:
Offline data collection status:  (0x00)	Offline data collection activity
					was never started.
					Auto Offline Data Collection: Disabled.
Self-test execution status:      (   0)	The previous self-test routine completed
					without error or no self-test has ever 
					been run.
Total time to complete Offline 
data collection: 		(    1) seconds.
Offline data collection
capabilities: 			 (0x59) SMART execute Offline immediate.
					No Auto Offline data collection support.
					Suspend Offline collection upon new
					command.
					Offline surface scan supported.
					Self-test supported.
					No Conveyance Self-test supported.
					Selective Self-test supported.
SMART capabilities:            (0x0002)	Does not save SMART data before
					entering power-saving mode.
					Supports SMART auto save timer.
Error logging capability:        (0x01)	Error logging supported.
					General Purpose Logging supported.
Short self-test routine 
recommended polling time: 	 (   1) minutes.
Extended self-test routine
recommended polling time: 	 (   2) minutes.

SMART Attributes Data Structure revision number: 10
Vendor Specific SMART Attributes with Thresholds:
ID# ATTRIBUTE_NAME          FLAG     VALUE WORST THRESH TYPE      UPDATED  WHEN_FAILED RAW_VALUE
  1 Raw_Read_Error_Rate     0x002f   100   100   050    Pre-fail  Always       -       0
  5 Reallocated_Sector_Ct   0x0033   100   100   010    Pre-fail  Always       -       10640
  9 Power_On_Hours          0x0032   100   100   000    Old_age   Always       -       2764
 12 Power_Cycle_Count       0x0032   100   100   000    Old_age   Always       -       25
161 Unknown_Attribute       0x0032   100   100   050    Old_age   Always       -       39
162 Unknown_Attribute       0x0032   100   100   000    Old_age   Always       -       2001996
163 Unknown_Attribute       0x0032   100   100   000    Old_age   Always       -       3000
164 Unknown_Attribute       0x0032   100   100   000    Old_age   Always       -       1226
166 Unknown_Attribute       0x0032   100   100   000    Old_age   Always       -       79
167 Unknown_Attribute       0x0032   100   100   000    Old_age   Always       -       0
168 Unknown_Attribute       0x0032   100   100   000    Old_age   Always       -       0
169 Unknown_Attribute       0x0032   100   100   000    Old_age   Always       -       60
171 Unknown_Attribute       0x0032   100   100   000    Old_age   Always       -       0
172 Unknown_Attribute       0x0032   100   100   000    Old_age   Always       -       0
174 Unknown_Attribute       0x0032   100   100   000    Old_age   Always       -       16
175 Program_Fail_Count_Chip 0x0032   100   100   000    Old_age   Always       -       1330
181 Program_Fail_Cnt_Total  0x0022   100   100   000    Old_age   Always       -       998
187 Reported_Uncorrect      0x0032   100   100   000    Old_age   Always       -       1330
194 Temperature_Celsius     0x0022   100   100   000    Old_age   Always       -       40
195 Hardware_ECC_Recovered  0x003a   100   100   000    Old_age   Always       -       6542845
196 Reallocated_Event_Count 0x0032   100   100   000    Old_age   Always       -       1330
199 UDMA_CRC_Error_Count    0x0032   100   100   000    Old_age   Always       -       1330
206 Unknown_SSD_Attribute   0x0032   100   100   000    Old_age   Always       -       1183
207 Unknown_SSD_Attribute   0x0032   100   100   000    Old_age   Always       -       1421
232 Available_Reservd_Space 0x0032   100   100   000    Old_age   Always       -       88
241 Total_LBAs_Written      0x0032   100   100   000    Old_age   Always       -       64960
242 Total_LBAs_Read         0x0032   100   100   000    Old_age   Always       -       62482
249 Unknown_Attribute       0x0032   100   100   000    Old_age   Always       -       94021
250 Read_Error_Retry_Rate   0x0032   100   100   000    Old_age   Always       -       65035

SMART Error Log Version: 1
ATA Error Count: 705 (device log contains only the most recent five errors)
	CR = Command Register [HEX]
	FR = Features Register [HEX]
	SC = Sector Count Register [HEX]
	SN = Sector Number Register [HEX]
	CL = Cylinder Low Register [HEX]
	CH = Cylinder High Register [HEX]
	DH = Device/Head Register [HEX]
	DC = Device Command Register [HEX]
	ER = Error register [HEX]
	ST = Status register [HEX]
Powered_Up_Time is measured from power on, and printed as
DDd+hh:mm:SS.sss where DD=days, hh=hours, mm=minutes,
SS=sec, and sss=millisec. It "wraps" after 49.710 days.

Error 705 occurred at disk power-on lifetime: 2764 hours (115 days + 4 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER ST SC SN CL CH DH
  -- -- -- -- -- -- --
  40 51 00 58 a2 91 40  Error: UNC at LBA = 0x0091a258 = 9544280

  Commands leading to the command that caused the error were:
  CR FR SC SN CL CH DH DC   Powered_Up_Time  Command/Feature_Name
  -- -- -- -- -- -- -- --  ----------------  --------------------
  25 00 00 d0 ac 60 e0 08  10d+19:42:15.154  READ DMA EXT
  c8 00 e8 e8 1d e0 ea 08  10d+19:42:14.034  READ DMA
  c8 00 28 a8 7e 80 ea 08  10d+19:42:13.471  READ DMA
  c8 00 00 d0 71 80 ea 08  10d+19:42:13.455  READ DMA
  b0 da 00 00 4f c2 00 08  10d+19:42:13.378  SMART RETURN STATUS

Error 704 occurred at disk power-on lifetime: 2764 hours (115 days + 4 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER ST SC SN CL CH DH
  -- -- -- -- -- -- --
  40 51 00 58 a2 91 40  Error: UNC at LBA = 0x0091a258 = 9544280

  Commands leading to the command that caused the error were:
  CR FR SC SN CL CH DH DC   Powered_Up_Time  Command/Feature_Name
  -- -- -- -- -- -- -- --  ----------------  --------------------
  c8 00 08 98 59 da ed 08  10d+19:39:49.502  READ DMA
  2f 00 01 30 08 00 a0 08  10d+19:39:49.255  READ LOG EXT
  2f 00 01 30 00 00 a0 08  10d+19:39:49.249  READ LOG EXT
  2f 00 01 00 00 00 a0 08  10d+19:39:49.243  READ LOG EXT
  ef 10 02 00 00 00 a0 08  10d+19:39:49.235  SET FEATURES [Enable SATA feature]

Error 703 occurred at disk power-on lifetime: 2764 hours (115 days + 4 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER ST SC SN CL CH DH
  -- -- -- -- -- -- --
  40 51 00 58 a2 91 40  Error: UNC at LBA = 0x0091a258 = 9544280

  Commands leading to the command that caused the error were:
  CR FR SC SN CL CH DH DC   Powered_Up_Time  Command/Feature_Name
  -- -- -- -- -- -- -- --  ----------------  --------------------
  c8 00 08 98 59 da ed 08  10d+19:39:49.175  READ DMA
  c8 00 08 90 59 da ed 08  10d+19:39:48.973  READ DMA
  c8 00 08 88 59 da ed 08  10d+19:39:48.973  READ DMA
  c8 00 08 80 59 da ed 08  10d+19:39:48.970  READ DMA
  c8 00 08 78 59 da ed 08  10d+19:39:48.969  READ DMA

Error 702 occurred at disk power-on lifetime: 2764 hours (115 days + 4 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER ST SC SN CL CH DH
  -- -- -- -- -- -- --
  40 51 00 58 a2 91 40  Error: UNC at LBA = 0x0091a258 = 9544280

  Commands leading to the command that caused the error were:
  CR FR SC SN CL CH DH DC   Powered_Up_Time  Command/Feature_Name
  -- -- -- -- -- -- -- --  ----------------  --------------------
  c8 00 08 08 6f ee ec 08  10d+19:38:14.804  READ DMA
  c8 00 08 00 6f ee ec 08  10d+19:38:14.437  READ DMA
  c8 00 08 f8 6e ee ec 08  10d+19:38:14.430  READ DMA
  c8 00 08 f0 6e ee ec 08  10d+19:38:14.335  READ DMA
  c8 00 08 e8 6e ee ec 08  10d+19:38:14.335  READ DMA

Error 701 occurred at disk power-on lifetime: 2764 hours (115 days + 4 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER ST SC SN CL CH DH
  -- -- -- -- -- -- --
  40 51 00 58 a2 91 40  Error: UNC at LBA = 0x0091a258 = 9544280

  Commands leading to the command that caused the error were:
  CR FR SC SN CL CH DH DC   Powered_Up_Time  Command/Feature_Name
  -- -- -- -- -- -- -- --  ----------------  --------------------
  c8 00 08 88 16 d9 ec 08  10d+19:37:54.957  READ DMA
  2f 00 01 30 08 00 a0 08  10d+19:37:54.768  READ LOG EXT
  2f 00 01 30 00 00 a0 08  10d+19:37:54.761  READ LOG EXT
  2f 00 01 00 00 00 a0 08  10d+19:37:54.756  READ LOG EXT
  ef 10 02 00 00 00 a0 08  10d+19:37:54.749  SET FEATURES [Enable SATA feature]

SMART Self-test log structure revision number 1
No self-tests have been logged.  [To run self-tests, use: smartctl -t]

SMART Selective self-test log data structure revision number 0
Note: revision number not 1 implies that no selective self-test has ever been run
 SPAN  MIN_LBA  MAX_LBA  CURRENT_TEST_STATUS
    1        0        0  Not_testing
    2        0        0  Not_testing
    3        0        0  Not_testing
    4        0        0  Not_testing
    5        0        0  Not_testing
Selective self-test flags (0x0):
  After scanning selected spans, do NOT read-scan remainder of disk.
If Selective self-test is pending on power-up, resume after 0 minute delay.

The above only provides legacy SMART information - try 'smartctl -x' for more


########## smartctl -x /dev/sda ##########
smartctl 7.4 2023-08-01 r5530 [x86_64-linux-6.5.7-200.fc38.x86_64] (local build)
Copyright (C) 2002-23, Bruce Allen, Christian Franke, www.smartmontools.org

=== START OF INFORMATION SECTION ===
Device Model:     ZM-SSD 128GB
Serial Number:    2301VE0R63330046
LU WWN Device Id: 0 000000 000000000
Firmware Version: VE0R6333
User Capacity:    128 035 676 160 bytes [128 GB]
Sector Size:      512 bytes logical/physical
Rotation Rate:    Solid State Device
Form Factor:      2.5 inches
TRIM Command:     Available, deterministic
Device is:        Not in smartctl database 7.3/5528
ATA Version is:   ACS-3, ATA8-ACS T13/1699-D revision 6
SATA Version is:  SATA 3.0, 6.0 Gb/s (current: 1.5 Gb/s)
Local Time is:    Tue Oct 24 09:13:28 2023 +07
SMART support is: Available - device has SMART capability.
SMART support is: Enabled
AAM feature is:   Unavailable
APM feature is:   Unavailable
Rd look-ahead is: Disabled
Write cache is:   Enabled
DSN feature is:   Unavailable
ATA Security is:  Disabled, frozen [SEC2]
Wt Cache Reorder: Unavailable

=== START OF READ SMART DATA SECTION ===
SMART overall-health self-assessment test result: PASSED

General SMART Values:
Offline data collection status:  (0x00)	Offline data collection activity
					was never started.
					Auto Offline Data Collection: Disabled.
Self-test execution status:      (   0)	The previous self-test routine completed
					without error or no self-test has ever 
					been run.
Total time to complete Offline 
data collection: 		(    1) seconds.
Offline data collection
capabilities: 			 (0x59) SMART execute Offline immediate.
					No Auto Offline data collection support.
					Suspend Offline collection upon new
					command.
					Offline surface scan supported.
					Self-test supported.
					No Conveyance Self-test supported.
					Selective Self-test supported.
SMART capabilities:            (0x0002)	Does not save SMART data before
					entering power-saving mode.
					Supports SMART auto save timer.
Error logging capability:        (0x01)	Error logging supported.
					General Purpose Logging supported.
Short self-test routine 
recommended polling time: 	 (   1) minutes.
Extended self-test routine
recommended polling time: 	 (   2) minutes.

SMART Attributes Data Structure revision number: 10
Vendor Specific SMART Attributes with Thresholds:
ID# ATTRIBUTE_NAME          FLAGS    VALUE WORST THRESH FAIL RAW_VALUE
  1 Raw_Read_Error_Rate     POSR-K   100   100   050    -    0
  5 Reallocated_Sector_Ct   PO--CK   100   100   010    -    10640
  9 Power_On_Hours          -O--CK   100   100   000    -    2764
 12 Power_Cycle_Count       -O--CK   100   100   000    -    25
161 Unknown_Attribute       -O--CK   100   100   050    -    39
162 Unknown_Attribute       -O--CK   100   100   000    -    2001996
163 Unknown_Attribute       -O--CK   100   100   000    -    3000
164 Unknown_Attribute       -O--CK   100   100   000    -    1226
166 Unknown_Attribute       -O--CK   100   100   000    -    79
167 Unknown_Attribute       -O--CK   100   100   000    -    0
168 Unknown_Attribute       -O--CK   100   100   000    -    0
169 Unknown_Attribute       -O--CK   100   100   000    -    60
171 Unknown_Attribute       -O--CK   100   100   000    -    0
172 Unknown_Attribute       -O--CK   100   100   000    -    0
174 Unknown_Attribute       -O--CK   100   100   000    -    16
175 Program_Fail_Count_Chip -O--CK   100   100   000    -    1330
181 Program_Fail_Cnt_Total  -O---K   100   100   000    -    998
187 Reported_Uncorrect      -O--CK   100   100   000    -    1330
194 Temperature_Celsius     -O---K   100   100   000    -    40
195 Hardware_ECC_Recovered  -O-RCK   100   100   000    -    6542845
196 Reallocated_Event_Count -O--CK   100   100   000    -    1330
199 UDMA_CRC_Error_Count    -O--CK   100   100   000    -    1330
206 Unknown_SSD_Attribute   -O--CK   100   100   000    -    1183
207 Unknown_SSD_Attribute   -O--CK   100   100   000    -    1421
232 Available_Reservd_Space -O--CK   100   100   000    -    88
241 Total_LBAs_Written      -O--CK   100   100   000    -    64960
242 Total_LBAs_Read         -O--CK   100   100   000    -    62482
249 Unknown_Attribute       -O--CK   100   100   000    -    94021
250 Read_Error_Retry_Rate   -O--CK   100   100   000    -    65035
                            ||||||_ K auto-keep
                            |||||__ C event count
                            ||||___ R error rate
                            |||____ S speed/performance
                            ||_____ O updated online
                            |______ P prefailure warning

General Purpose Log Directory Version 1
SMART           Log Directory Version 1 [multi-sector log support]
Address    Access  R/W   Size  Description
0x00       GPL,SL  R/O      1  Log Directory
0x01           SL  R/O      1  Summary SMART error log
0x02           SL  R/O      1  Comprehensive SMART error log
0x03       GPL     R/O      2  Ext. Comprehensive SMART error log
0x04       GPL,SL  R/O      5  Device Statistics log
0x06           SL  R/O      1  SMART self-test log
0x07       GPL     R/O      1  Extended self-test log
0x09           SL  R/W      1  Selective self-test log
0x10       GPL     R/O      1  NCQ Command Error log
0x11       GPL     R/O      1  SATA Phy Event Counters log
0x30       GPL,SL  R/O      8  IDENTIFY DEVICE data log
0x80-0x9f  GPL,SL  R/W     16  Host vendor specific log
0xe0       GPL,SL  R/W      1  SCT Command/Status
0xe1       GPL,SL  R/W      1  SCT Data Transfer

SMART Extended Comprehensive Error Log Version: 1 (2 sectors)
Device Error Count: 705 (device log contains only the most recent 8 errors)
	CR     = Command Register
	FEATR  = Features Register
	COUNT  = Count (was: Sector Count) Register
	LBA_48 = Upper bytes of LBA High/Mid/Low Registers ]  ATA-8
	LH     = LBA High (was: Cylinder High) Register    ]   LBA
	LM     = LBA Mid (was: Cylinder Low) Register      ] Register
	LL     = LBA Low (was: Sector Number) Register     ]
	DV     = Device (was: Device/Head) Register
	DC     = Device Control Register
	ER     = Error register
	ST     = Status register
Powered_Up_Time is measured from power on, and printed as
DDd+hh:mm:SS.sss where DD=days, hh=hours, mm=minutes,
SS=sec, and sss=millisec. It "wraps" after 49.710 days.

Error 705 [7] occurred at disk power-on lifetime: 2764 hours (115 days + 4 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 51 00 00 00 00 02 91 a2 58 40 08  Error: UNC at LBA = 0x0291a258 = 43098712

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  25 00 00 0a 00 00 00 06 60 ac d0 e0 08 10d+19:42:15.154  READ DMA EXT
  c8 00 00 00 e8 00 00 00 e0 1d e8 ea 08 10d+19:42:14.034  READ DMA
  c8 00 00 00 28 00 00 00 80 7e a8 ea 08 10d+19:42:13.471  READ DMA
  c8 00 00 00 00 00 00 00 80 71 d0 ea 08 10d+19:42:13.455  READ DMA
  b0 00 da 00 00 00 00 00 c2 4f 00 00 08 10d+19:42:13.378  SMART RETURN STATUS

Error 704 [6] occurred at disk power-on lifetime: 2764 hours (115 days + 4 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 51 00 00 00 00 02 91 a2 58 40 08  Error: UNC at LBA = 0x0291a258 = 43098712

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  c8 00 00 00 08 00 00 00 da 59 98 ed 08 10d+19:39:49.502  READ DMA
  2f 00 00 00 01 00 00 00 00 08 30 a0 08 10d+19:39:49.255  READ LOG EXT
  2f 00 00 00 01 00 00 00 00 00 30 a0 08 10d+19:39:49.249  READ LOG EXT
  2f 00 00 00 01 00 00 00 00 00 00 a0 08 10d+19:39:49.243  READ LOG EXT
  ef 00 10 00 02 00 00 00 00 00 00 a0 08 10d+19:39:49.235  SET FEATURES [Enable SATA feature]

Error 703 [5] occurred at disk power-on lifetime: 2764 hours (115 days + 4 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 51 00 00 00 00 02 91 a2 58 40 08  Error: UNC at LBA = 0x0291a258 = 43098712

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  c8 00 00 00 08 00 00 00 da 59 98 ed 08 10d+19:39:49.175  READ DMA
  c8 00 00 00 08 00 00 00 da 59 90 ed 08 10d+19:39:48.973  READ DMA
  c8 00 00 00 08 00 00 00 da 59 88 ed 08 10d+19:39:48.973  READ DMA
  c8 00 00 00 08 00 00 00 da 59 80 ed 08 10d+19:39:48.970  READ DMA
  c8 00 00 00 08 00 00 00 da 59 78 ed 08 10d+19:39:48.969  READ DMA

Error 702 [4] occurred at disk power-on lifetime: 2764 hours (115 days + 4 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 51 00 00 00 00 02 91 a2 58 40 08  Error: UNC at LBA = 0x0291a258 = 43098712

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  c8 00 00 00 08 00 00 00 ee 6f 08 ec 08 10d+19:38:14.804  READ DMA
  c8 00 00 00 08 00 00 00 ee 6f 00 ec 08 10d+19:38:14.437  READ DMA
  c8 00 00 00 08 00 00 00 ee 6e f8 ec 08 10d+19:38:14.430  READ DMA
  c8 00 00 00 08 00 00 00 ee 6e f0 ec 08 10d+19:38:14.335  READ DMA
  c8 00 00 00 08 00 00 00 ee 6e e8 ec 08 10d+19:38:14.335  READ DMA

Error 701 [3] occurred at disk power-on lifetime: 2764 hours (115 days + 4 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 51 00 00 00 00 02 91 a2 58 40 08  Error: UNC at LBA = 0x0291a258 = 43098712

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  c8 00 00 00 08 00 00 00 d9 16 88 ec 08 10d+19:37:54.957  READ DMA
  2f 00 00 00 01 00 00 00 00 08 30 a0 08 10d+19:37:54.768  READ LOG EXT
  2f 00 00 00 01 00 00 00 00 00 30 a0 08 10d+19:37:54.761  READ LOG EXT
  2f 00 00 00 01 00 00 00 00 00 00 a0 08 10d+19:37:54.756  READ LOG EXT
  ef 00 10 00 02 00 00 00 00 00 00 a0 08 10d+19:37:54.749  SET FEATURES [Enable SATA feature]

Error 700 [2] occurred at disk power-on lifetime: 2764 hours (115 days + 4 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 51 00 00 00 00 02 91 a2 58 40 08  Error: UNC at LBA = 0x0291a258 = 43098712

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  c8 00 00 00 08 00 00 00 d9 16 88 ec 08 10d+19:37:54.693  READ DMA
  c8 00 00 00 08 00 00 00 d9 16 80 ec 08 10d+19:37:54.532  READ DMA
  c8 00 00 00 08 00 00 00 d9 16 78 ec 08 10d+19:37:54.527  READ DMA
  c8 00 00 00 08 00 00 00 d9 16 70 ec 08 10d+19:37:54.526  READ DMA
  c8 00 00 00 08 00 00 00 d9 16 68 ec 08 10d+19:37:54.526  READ DMA

Error 699 [1] occurred at disk power-on lifetime: 2764 hours (115 days + 4 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 51 00 00 00 00 02 91 a2 58 40 08  Error: UNC at LBA = 0x0291a258 = 43098712

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  c8 00 00 00 08 00 00 00 7c ee f0 eb 08 10d+19:35:14.056  READ DMA
  2f 00 00 00 01 00 00 00 00 08 30 a0 08 10d+19:35:13.842  READ LOG EXT
  2f 00 00 00 01 00 00 00 00 00 30 a0 08 10d+19:35:13.836  READ LOG EXT
  2f 00 00 00 01 00 00 00 00 00 00 a0 08 10d+19:35:13.829  READ LOG EXT
  ef 00 10 00 02 00 00 00 00 00 00 a0 08 10d+19:35:13.821  SET FEATURES [Enable SATA feature]

Error 698 [0] occurred at disk power-on lifetime: 2764 hours (115 days + 4 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 51 00 00 00 00 02 91 a2 58 40 08  Error: UNC at LBA = 0x0291a258 = 43098712

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  c8 00 00 00 08 00 00 00 7c ee f0 eb 08 10d+19:35:13.746  READ DMA
  2f 00 00 00 01 00 00 00 00 08 30 a0 08 10d+19:35:13.550  READ LOG EXT
  2f 00 00 00 01 00 00 00 00 00 30 a0 08 10d+19:35:13.545  READ LOG EXT
  2f 00 00 00 01 00 00 00 00 00 00 a0 08 10d+19:35:13.539  READ LOG EXT
  ef 00 10 00 02 00 00 00 00 00 00 a0 08 10d+19:35:13.531  SET FEATURES [Enable SATA feature]

SMART Extended Self-test Log Version: 1 (1 sectors)
No self-tests have been logged.  [To run self-tests, use: smartctl -t]

SMART Selective self-test log data structure revision number 0
Note: revision number not 1 implies that no selective self-test has ever been run
 SPAN  MIN_LBA  MAX_LBA  CURRENT_TEST_STATUS
    1        0        0  Not_testing
    2        0        0  Not_testing
    3        0        0  Not_testing
    4        0        0  Not_testing
    5        0        0  Not_testing
Selective self-test flags (0x0):
  After scanning selected spans, do NOT read-scan remainder of disk.
If Selective self-test is pending on power-up, resume after 0 minute delay.

SCT Commands not supported

Device Statistics (GP Log 0x04)
Page  Offset Size        Value Flags Description
0x01  =====  =               =  ===  == General Statistics (rev 1) ==
0x01  0x008  4              25  ---  Lifetime Power-On Resets
0x01  0x010  4            2764  ---  Power-on Hours
0x01  0x018  6    136231967755  ---  Logical Sectors Written
0x01  0x020  6       551029973  ---  Number of Write Commands
0x01  0x028  6    131036010017  ---  Logical Sectors Read
0x01  0x030  6       524410352  ---  Number of Read Commands
0x01  0x038  6      9951436563  ---  Date and Time TimeStamp
0x04  =====  =               =  ===  == General Errors Statistics (rev 1) ==
0x04  0x008  4             826  ---  Number of Reported Uncorrectable Errors
0x04  0x010  4              86  ---  Resets Between Cmd Acceptance and Completion
0x06  =====  =               =  ===  == Transport Statistics (rev 1) ==
0x06  0x008  4             312  ---  Number of Hardware Resets
0x06  0x018  4               0  ---  Number of Interface CRC Errors
0x07  =====  =               =  ===  == Solid State Device Statistics (rev 1) ==
0x07  0x008  1              74  ---  Percentage Used Endurance Indicator
                                |||_ C monitored condition met
                                ||__ D supports DSN
                                |___ N normalized value

Pending Defects log (GP Log 0x0c) not supported

SATA Phy Event Counters (GP Log 0x11)
ID      Size     Value  Description
0x0001  4            0  Command failed due to ICRC error
0x000a  4          146  Device-to-host register FISes sent due to a COMRESET

