########## smartctl -a /dev/sdd ##########
smartctl 7.2 2020-12-30 r5155 [x86_64-linux-5.15.107-2-pve] (local build)
Copyright (C) 2002-20, Bruce Allen, Christian Franke, www.smartmontools.org

=== START OF INFORMATION SECTION ===
Device Model:     ZM-SSD 128GB
Serial Number:    2301VE0R63330046
LU WWN Device Id: 0 000000 000000000
Firmware Version: VE0R6333
User Capacity:    128,035,676,160 bytes [128 GB]
Sector Size:      512 bytes logical/physical
Rotation Rate:    Solid State Device
Form Factor:      2.5 inches
TRIM Command:     Available, deterministic
Device is:        Not in smartctl database [for details use: -P showall]
ATA Version is:   ACS-3, ATA8-ACS T13/1699-D revision 6
SATA Version is:  SATA 3.0, 6.0 Gb/s (current: 6.0 Gb/s)
Local Time is:    Fri Jun  9 07:23:17 2023 +07
SMART support is: Available - device has SMART capability.
SMART support is: Enabled

=== START OF READ SMART DATA SECTION ===
SMART overall-health self-assessment test result: PASSED

General SMART Values:
Offline data collection status:  (0x00)	Offline data collection activity
					was never started.
					Auto Offline Data Collection: Disabled.
Self-test execution status:      (   0)	The previous self-test routine completed
					without error or no self-test has ever 
					been run.
Total time to complete Offline 
data collection: 		(    1) seconds.
Offline data collection
capabilities: 			 (0x59) SMART execute Offline immediate.
					No Auto Offline data collection support.
					Suspend Offline collection upon new
					command.
					Offline surface scan supported.
					Self-test supported.
					No Conveyance Self-test supported.
					Selective Self-test supported.
SMART capabilities:            (0x0002)	Does not save SMART data before
					entering power-saving mode.
					Supports SMART auto save timer.
Error logging capability:        (0x01)	Error logging supported.
					General Purpose Logging supported.
Short self-test routine 
recommended polling time: 	 (   1) minutes.
Extended self-test routine
recommended polling time: 	 (   2) minutes.

SMART Attributes Data Structure revision number: 10
Vendor Specific SMART Attributes with Thresholds:
ID# ATTRIBUTE_NAME          FLAG     VALUE WORST THRESH TYPE      UPDATED  WHEN_FAILED RAW_VALUE
  1 Raw_Read_Error_Rate     0x002f   100   100   050    Pre-fail  Always       -       0
  5 Reallocated_Sector_Ct   0x0033   100   100   010    Pre-fail  Always       -       144
  9 Power_On_Hours          0x0032   100   100   000    Old_age   Always       -       595
 12 Power_Cycle_Count       0x0032   100   100   000    Old_age   Always       -       16
161 Unknown_Attribute       0x0032   100   100   050    Old_age   Always       -       1
162 Unknown_Attribute       0x0032   100   100   000    Old_age   Always       -       974215
163 Unknown_Attribute       0x0032   100   100   000    Old_age   Always       -       3000
164 Unknown_Attribute       0x0032   100   100   000    Old_age   Always       -       596
166 Unknown_Attribute       0x0032   100   100   000    Old_age   Always       -       41
167 Unknown_Attribute       0x0032   100   100   000    Old_age   Always       -       0
168 Unknown_Attribute       0x0032   100   100   000    Old_age   Always       -       0
169 Unknown_Attribute       0x0032   100   100   000    Old_age   Always       -       81
171 Unknown_Attribute       0x0032   100   100   000    Old_age   Always       -       0
172 Unknown_Attribute       0x0032   100   100   000    Old_age   Always       -       0
174 Unknown_Attribute       0x0032   100   100   000    Old_age   Always       -       10
175 Program_Fail_Count_Chip 0x0032   100   100   000    Old_age   Always       -       18
181 Program_Fail_Cnt_Total  0x0022   100   100   000    Old_age   Always       -       178
187 Reported_Uncorrect      0x0032   100   100   000    Old_age   Always       -       18
194 Temperature_Celsius     0x0022   100   100   000    Old_age   Always       -       40
195 Hardware_ECC_Recovered  0x003a   100   100   000    Old_age   Always       -       34291
196 Reallocated_Event_Count 0x0032   100   100   000    Old_age   Always       -       18
199 UDMA_CRC_Error_Count    0x0032   100   100   000    Old_age   Always       -       18
206 Unknown_SSD_Attribute   0x0032   100   100   000    Old_age   Always       -       553
207 Unknown_SSD_Attribute   0x0032   100   100   000    Old_age   Always       -       692
232 Available_Reservd_Space 0x0032   100   100   000    Old_age   Always       -       99
241 Total_LBAs_Written      0x0032   100   100   000    Old_age   Always       -       30970
242 Total_LBAs_Read         0x0032   100   100   000    Old_age   Always       -       29546
249 Unknown_Attribute       0x0032   100   100   000    Old_age   Always       -       47048
250 Read_Error_Retry_Rate   0x0032   100   100   000    Old_age   Always       -       31037

SMART Error Log Version: 1
ATA Error Count: 15 (device log contains only the most recent five errors)
	CR = Command Register [HEX]
	FR = Features Register [HEX]
	SC = Sector Count Register [HEX]
	SN = Sector Number Register [HEX]
	CL = Cylinder Low Register [HEX]
	CH = Cylinder High Register [HEX]
	DH = Device/Head Register [HEX]
	DC = Device Command Register [HEX]
	ER = Error register [HEX]
	ST = Status register [HEX]
Powered_Up_Time is measured from power on, and printed as
DDd+hh:mm:SS.sss where DD=days, hh=hours, mm=minutes,
SS=sec, and sss=millisec. It "wraps" after 49.710 days.

Error 15 occurred at disk power-on lifetime: 584 hours (24 days + 8 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER ST SC SN CL CH DH
  -- -- -- -- -- -- --
  40 41 10 f0 30 98 40  Error: UNC

  Commands leading to the command that caused the error were:
  CR FR SC SN CL CH DH DC   Powered_Up_Time  Command/Feature_Name
  -- -- -- -- -- -- -- --  ----------------  --------------------
  2f 00 01 30 08 00 a0 08   2d+08:05:28.243  READ LOG EXT
  2f 00 01 30 08 00 a0 08   2d+08:05:27.950  READ LOG EXT
  2f 00 01 30 00 00 a0 08   2d+08:05:27.915  READ LOG EXT
  2f 00 01 00 00 00 a0 08   2d+08:05:27.885  READ LOG EXT
  ef 10 02 00 00 00 a0 08   2d+08:05:27.858  SET FEATURES [Enable SATA feature]

Error 14 occurred at disk power-on lifetime: 584 hours (24 days + 8 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER ST SC SN CL CH DH
  -- -- -- -- -- -- --
  40 41 58 f0 30 98 40  Error: UNC

  Commands leading to the command that caused the error were:
  CR FR SC SN CL CH DH DC   Powered_Up_Time  Command/Feature_Name
  -- -- -- -- -- -- -- --  ----------------  --------------------
  2f 00 01 30 08 00 a0 08   2d+08:05:27.596  READ LOG EXT
  2f 00 01 30 08 00 a0 08   2d+08:05:27.402  READ LOG EXT
  2f 00 01 30 00 00 a0 08   2d+08:05:27.375  READ LOG EXT
  2f 00 01 00 00 00 a0 08   2d+08:05:27.345  READ LOG EXT
  ef 10 02 00 00 00 a0 08   2d+08:05:27.313  SET FEATURES [Enable SATA feature]

Error 13 occurred at disk power-on lifetime: 584 hours (24 days + 8 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER ST SC SN CL CH DH
  -- -- -- -- -- -- --
  40 41 80 f0 30 98 40

  Commands leading to the command that caused the error were:
  CR FR SC SN CL CH DH DC   Powered_Up_Time  Command/Feature_Name
  -- -- -- -- -- -- -- --  ----------------  --------------------
  e7 00 00 00 00 00 a0 08   2d+08:05:27.112  FLUSH CACHE
  60 08 78 e8 30 98 40 08   2d+08:05:26.932  READ FPDMA QUEUED
  60 08 70 e0 30 98 40 08   2d+08:05:26.932  READ FPDMA QUEUED
  60 08 68 d8 30 98 40 08   2d+08:05:26.932  READ FPDMA QUEUED
  60 08 60 d0 30 98 40 08   2d+08:05:26.931  READ FPDMA QUEUED

Error 12 occurred at disk power-on lifetime: 584 hours (24 days + 8 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER ST SC SN CL CH DH
  -- -- -- -- -- -- --
  40 41 00 00 00 00 00  Error: 

  Commands leading to the command that caused the error were:
  CR FR SC SN CL CH DH DC   Powered_Up_Time  Command/Feature_Name
  -- -- -- -- -- -- -- --  ----------------  --------------------
  00 00 00 00 00 00 00 00   2d+08:04:29.188  NOP [Abort queued commands]
  60 80 f0 80 c7 f8 40 08   2d+08:04:28.583  READ FPDMA QUEUED
  60 80 e8 80 07 11 40 08      00:00:00.000  READ FPDMA QUEUED
  60 80 a0 80 47 1f 40 08   2d+08:04:28.321  READ FPDMA QUEUED
  60 80 d8 80 07 06 40 08   2d+08:04:28.075  READ FPDMA QUEUED

Error 11 occurred at disk power-on lifetime: 572 hours (23 days + 20 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER ST SC SN CL CH DH
  -- -- -- -- -- -- --
  40 41 20 98 0d c7 40  Error: UNC

  Commands leading to the command that caused the error were:
  CR FR SC SN CL CH DH DC   Powered_Up_Time  Command/Feature_Name
  -- -- -- -- -- -- -- --  ----------------  --------------------
  2f 00 01 30 08 00 a0 08   1d+19:56:16.551  READ LOG EXT
  60 08 18 90 0d c7 40 08   1d+19:56:16.374  READ FPDMA QUEUED
  60 08 70 88 0d c7 40 08   1d+19:56:16.317  READ FPDMA QUEUED
  60 08 d8 80 0d c7 40 08   1d+19:56:16.297  READ FPDMA QUEUED
  2f 00 01 30 08 00 a0 08   1d+19:56:16.295  READ LOG EXT

SMART Self-test log structure revision number 1
No self-tests have been logged.  [To run self-tests, use: smartctl -t]

SMART Selective self-test log data structure revision number 0
Note: revision number not 1 implies that no selective self-test has ever been run
 SPAN  MIN_LBA  MAX_LBA  CURRENT_TEST_STATUS
    1        0        0  Not_testing
    2        0        0  Not_testing
    3        0        0  Not_testing
    4        0        0  Not_testing
    5        0        0  Not_testing
Selective self-test flags (0x0):
  After scanning selected spans, do NOT read-scan remainder of disk.
If Selective self-test is pending on power-up, resume after 0 minute delay.


########## smartctl -x /dev/sdd ##########
smartctl 7.2 2020-12-30 r5155 [x86_64-linux-5.15.107-2-pve] (local build)
Copyright (C) 2002-20, Bruce Allen, Christian Franke, www.smartmontools.org

=== START OF INFORMATION SECTION ===
Device Model:     ZM-SSD 128GB
Serial Number:    2301VE0R63330046
LU WWN Device Id: 0 000000 000000000
Firmware Version: VE0R6333
User Capacity:    128,035,676,160 bytes [128 GB]
Sector Size:      512 bytes logical/physical
Rotation Rate:    Solid State Device
Form Factor:      2.5 inches
TRIM Command:     Available, deterministic
Device is:        Not in smartctl database [for details use: -P showall]
ATA Version is:   ACS-3, ATA8-ACS T13/1699-D revision 6
SATA Version is:  SATA 3.0, 6.0 Gb/s (current: 6.0 Gb/s)
Local Time is:    Fri Jun  9 07:23:17 2023 +07
SMART support is: Available - device has SMART capability.
SMART support is: Enabled
AAM feature is:   Unavailable
APM feature is:   Unavailable
Rd look-ahead is: Disabled
Write cache is:   Enabled
DSN feature is:   Unavailable
ATA Security is:  Disabled, frozen [SEC2]
Wt Cache Reorder: Unavailable

=== START OF READ SMART DATA SECTION ===
SMART overall-health self-assessment test result: PASSED

General SMART Values:
Offline data collection status:  (0x00)	Offline data collection activity
					was never started.
					Auto Offline Data Collection: Disabled.
Self-test execution status:      (   0)	The previous self-test routine completed
					without error or no self-test has ever 
					been run.
Total time to complete Offline 
data collection: 		(    1) seconds.
Offline data collection
capabilities: 			 (0x59) SMART execute Offline immediate.
					No Auto Offline data collection support.
					Suspend Offline collection upon new
					command.
					Offline surface scan supported.
					Self-test supported.
					No Conveyance Self-test supported.
					Selective Self-test supported.
SMART capabilities:            (0x0002)	Does not save SMART data before
					entering power-saving mode.
					Supports SMART auto save timer.
Error logging capability:        (0x01)	Error logging supported.
					General Purpose Logging supported.
Short self-test routine 
recommended polling time: 	 (   1) minutes.
Extended self-test routine
recommended polling time: 	 (   2) minutes.

SMART Attributes Data Structure revision number: 10
Vendor Specific SMART Attributes with Thresholds:
ID# ATTRIBUTE_NAME          FLAGS    VALUE WORST THRESH FAIL RAW_VALUE
  1 Raw_Read_Error_Rate     POSR-K   100   100   050    -    0
  5 Reallocated_Sector_Ct   PO--CK   100   100   010    -    144
  9 Power_On_Hours          -O--CK   100   100   000    -    595
 12 Power_Cycle_Count       -O--CK   100   100   000    -    16
161 Unknown_Attribute       -O--CK   100   100   050    -    1
162 Unknown_Attribute       -O--CK   100   100   000    -    974215
163 Unknown_Attribute       -O--CK   100   100   000    -    3000
164 Unknown_Attribute       -O--CK   100   100   000    -    596
166 Unknown_Attribute       -O--CK   100   100   000    -    41
167 Unknown_Attribute       -O--CK   100   100   000    -    0
168 Unknown_Attribute       -O--CK   100   100   000    -    0
169 Unknown_Attribute       -O--CK   100   100   000    -    81
171 Unknown_Attribute       -O--CK   100   100   000    -    0
172 Unknown_Attribute       -O--CK   100   100   000    -    0
174 Unknown_Attribute       -O--CK   100   100   000    -    10
175 Program_Fail_Count_Chip -O--CK   100   100   000    -    18
181 Program_Fail_Cnt_Total  -O---K   100   100   000    -    178
187 Reported_Uncorrect      -O--CK   100   100   000    -    18
194 Temperature_Celsius     -O---K   100   100   000    -    40
195 Hardware_ECC_Recovered  -O-RCK   100   100   000    -    34291
196 Reallocated_Event_Count -O--CK   100   100   000    -    18
199 UDMA_CRC_Error_Count    -O--CK   100   100   000    -    18
206 Unknown_SSD_Attribute   -O--CK   100   100   000    -    553
207 Unknown_SSD_Attribute   -O--CK   100   100   000    -    692
232 Available_Reservd_Space -O--CK   100   100   000    -    99
241 Total_LBAs_Written      -O--CK   100   100   000    -    30970
242 Total_LBAs_Read         -O--CK   100   100   000    -    29546
249 Unknown_Attribute       -O--CK   100   100   000    -    47048
250 Read_Error_Retry_Rate   -O--CK   100   100   000    -    31037
                            ||||||_ K auto-keep
                            |||||__ C event count
                            ||||___ R error rate
                            |||____ S speed/performance
                            ||_____ O updated online
                            |______ P prefailure warning

General Purpose Log Directory Version 1
SMART           Log Directory Version 1 [multi-sector log support]
Address    Access  R/W   Size  Description
0x00       GPL,SL  R/O      1  Log Directory
0x01           SL  R/O      1  Summary SMART error log
0x02           SL  R/O      1  Comprehensive SMART error log
0x03       GPL     R/O      2  Ext. Comprehensive SMART error log
0x04       GPL,SL  R/O      5  Device Statistics log
0x06           SL  R/O      1  SMART self-test log
0x07       GPL     R/O      1  Extended self-test log
0x09           SL  R/W      1  Selective self-test log
0x10       GPL     R/O      1  NCQ Command Error log
0x11       GPL     R/O      1  SATA Phy Event Counters log
0x30       GPL,SL  R/O      8  IDENTIFY DEVICE data log
0x80-0x9f  GPL,SL  R/W     16  Host vendor specific log
0xe0       GPL,SL  R/W      1  SCT Command/Status
0xe1       GPL,SL  R/W      1  SCT Data Transfer

SMART Extended Comprehensive Error Log Version: 1 (2 sectors)
Device Error Count: 15 (device log contains only the most recent 8 errors)
	CR     = Command Register
	FEATR  = Features Register
	COUNT  = Count (was: Sector Count) Register
	LBA_48 = Upper bytes of LBA High/Mid/Low Registers ]  ATA-8
	LH     = LBA High (was: Cylinder High) Register    ]   LBA
	LM     = LBA Mid (was: Cylinder Low) Register      ] Register
	LL     = LBA Low (was: Sector Number) Register     ]
	DV     = Device (was: Device/Head) Register
	DC     = Device Control Register
	ER     = Error register
	ST     = Status register
Powered_Up_Time is measured from power on, and printed as
DDd+hh:mm:SS.sss where DD=days, hh=hours, mm=minutes,
SS=sec, and sss=millisec. It "wraps" after 49.710 days.

Error 15 [7] occurred at disk power-on lifetime: 584 hours (24 days + 8 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 41 00 10 00 00 03 98 30 f0 40 08  Error: UNC

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  2f 00 00 00 01 00 00 00 00 08 30 a0 08  2d+08:05:28.243  READ LOG EXT
  2f 00 00 00 01 00 00 00 00 08 30 a0 08  2d+08:05:27.950  READ LOG EXT
  2f 00 00 00 01 00 00 00 00 00 30 a0 08  2d+08:05:27.915  READ LOG EXT
  2f 00 00 00 01 00 00 00 00 00 00 a0 08  2d+08:05:27.885  READ LOG EXT
  ef 00 10 00 02 00 00 00 00 00 00 a0 08  2d+08:05:27.858  SET FEATURES [Enable SATA feature]

Error 14 [6] occurred at disk power-on lifetime: 584 hours (24 days + 8 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 41 00 58 00 00 03 98 30 f0 40 08  Error: UNC

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  2f 00 00 00 01 00 00 00 00 08 30 a0 08  2d+08:05:27.596  READ LOG EXT
  2f 00 00 00 01 00 00 00 00 08 30 a0 08  2d+08:05:27.402  READ LOG EXT
  2f 00 00 00 01 00 00 00 00 00 30 a0 08  2d+08:05:27.375  READ LOG EXT
  2f 00 00 00 01 00 00 00 00 00 00 a0 08  2d+08:05:27.345  READ LOG EXT
  ef 00 10 00 02 00 00 00 00 00 00 a0 08  2d+08:05:27.313  SET FEATURES [Enable SATA feature]

Error 13 [5] occurred at disk power-on lifetime: 584 hours (24 days + 8 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 41 00 80 00 00 03 98 30 f0 40 08

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  e7 00 00 00 00 00 00 00 00 00 00 a0 08  2d+08:05:27.112  FLUSH CACHE
  60 00 08 00 78 00 00 03 98 30 e8 40 08  2d+08:05:26.932  READ FPDMA QUEUED
  60 00 08 00 70 00 00 03 98 30 e0 40 08  2d+08:05:26.932  READ FPDMA QUEUED
  60 00 08 00 68 00 00 03 98 30 d8 40 08  2d+08:05:26.932  READ FPDMA QUEUED
  60 00 08 00 60 00 00 03 98 30 d0 40 08  2d+08:05:26.931  READ FPDMA QUEUED

Error 12 [4] occurred at disk power-on lifetime: 584 hours (24 days + 8 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 41 00 00 00 00 00 00 00 00 00 00  Error: 

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  00 00 00 00 00 00 00 00 00 00 00 00 00  2d+08:04:29.188  NOP [Abort queued commands]
  60 00 80 00 f0 00 00 0c f8 c7 80 40 08  2d+08:04:28.583  READ FPDMA QUEUED
  60 00 80 00 e8 00 00 09 11 07 80 40 08     00:00:00.000  READ FPDMA QUEUED
  60 00 80 00 a0 00 00 0b 1f 47 80 40 08  2d+08:04:28.321  READ FPDMA QUEUED
  60 00 80 00 d8 00 00 09 06 07 80 40 08  2d+08:04:28.075  READ FPDMA QUEUED

Error 11 [3] occurred at disk power-on lifetime: 572 hours (23 days + 20 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 41 00 20 00 00 0c c7 0d 98 40 08  Error: UNC

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  2f 00 00 00 01 00 00 00 00 08 30 a0 08  1d+19:56:16.551  READ LOG EXT
  60 00 08 00 18 00 00 0c c7 0d 90 40 08  1d+19:56:16.374  READ FPDMA QUEUED
  60 00 08 00 70 00 00 0c c7 0d 88 40 08  1d+19:56:16.317  READ FPDMA QUEUED
  60 00 08 00 d8 00 00 0c c7 0d 80 40 08  1d+19:56:16.297  READ FPDMA QUEUED
  2f 00 00 00 01 00 00 00 00 08 30 a0 08  1d+19:56:16.295  READ LOG EXT

Error 10 [2] occurred at disk power-on lifetime: 572 hours (23 days + 20 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 41 00 00 00 00 00 00 00 00 00 00  Error: 

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  00 00 00 00 00 00 00 00 00 00 00 00 00  1d+19:56:16.195  NOP [Abort queued commands]
  60 00 80 00 c0 00 00 09 2d 87 80 40 08  1d+19:56:15.829  READ FPDMA QUEUED
  60 00 80 00 80 00 00 0c c4 87 80 40 08  1d+19:56:15.790  READ FPDMA QUEUED
  60 00 80 00 70 00 00 09 24 07 80 40 08  1d+19:56:15.750  READ FPDMA QUEUED
  60 00 80 00 30 00 00 09 29 c7 80 40 08  1d+19:56:15.711  READ FPDMA QUEUED

Error 9 [1] occurred at disk power-on lifetime: 553 hours (23 days + 1 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 41 00 78 00 00 09 63 47 c0 40 08  Error: UNC

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  2f 00 00 00 01 00 00 00 00 08 30 a0 08  1d+01:25:56.366  READ LOG EXT
  60 00 08 00 70 00 00 09 63 47 b8 40 08  1d+01:25:56.312  READ FPDMA QUEUED
  60 00 08 00 68 00 00 09 63 47 b0 40 08  1d+01:25:56.312  READ FPDMA QUEUED
  60 00 08 00 60 00 00 09 63 47 a8 40 08  1d+01:25:56.311  READ FPDMA QUEUED
  60 00 08 00 58 00 00 09 63 47 a0 40 08  1d+01:25:56.311  READ FPDMA QUEUED

Error 8 [0] occurred at disk power-on lifetime: 553 hours (23 days + 1 hours)
  When the command that caused the error occurred, the device was active or idle.

  After command completion occurred, registers were:
  ER -- ST COUNT  LBA_48  LH LM LL DV DC
  -- -- -- == -- == == == -- -- -- -- --
  40 -- 41 00 00 00 00 00 00 00 00 00 00  Error: 

  Commands leading to the command that caused the error were:
  CR FEATR COUNT  LBA_48  LH LM LL DV DC  Powered_Up_Time  Command/Feature_Name
  -- == -- == -- == == == -- -- -- -- --  ---------------  --------------------
  00 00 00 00 00 00 00 00 00 00 00 00 00  1d+01:25:55.848  NOP [Abort queued commands]
  60 00 80 00 40 00 00 09 b6 c7 80 40 08  1d+01:25:55.117  READ FPDMA QUEUED
  60 00 80 00 80 00 00 09 5e 07 80 40 08  1d+01:25:54.917  READ FPDMA QUEUED
  60 00 80 00 f0 00 00 08 58 c7 80 40 08  1d+01:25:54.698  READ FPDMA QUEUED
  60 00 80 00 70 00 00 09 b2 87 80 40 08  1d+01:25:54.558  READ FPDMA QUEUED

SMART Extended Self-test Log Version: 1 (1 sectors)
No self-tests have been logged.  [To run self-tests, use: smartctl -t]

SMART Selective self-test log data structure revision number 0
Note: revision number not 1 implies that no selective self-test has ever been run
 SPAN  MIN_LBA  MAX_LBA  CURRENT_TEST_STATUS
    1        0        0  Not_testing
    2        0        0  Not_testing
    3        0        0  Not_testing
    4        0        0  Not_testing
    5        0        0  Not_testing
Selective self-test flags (0x0):
  After scanning selected spans, do NOT read-scan remainder of disk.
If Selective self-test is pending on power-up, resume after 0 minute delay.

SCT Commands not supported

Device Statistics (GP Log 0x04)
Page  Offset Size        Value Flags Description
0x01  =====  =               =  ===  == General Statistics (rev 1) ==
0x01  0x008  4              16  ---  Lifetime Power-On Resets
0x01  0x010  4             595  ---  Power-on Hours
0x01  0x018  6     64949392447  ---  Logical Sectors Written
0x01  0x020  6       261083419  ---  Number of Write Commands
0x01  0x028  6     61963001207  ---  Logical Sectors Read
0x01  0x030  6       243244565  ---  Number of Read Commands
0x01  0x038  6      2144420878  ---  Date and Time TimeStamp
0x04  =====  =               =  ===  == General Errors Statistics (rev 1) ==
0x04  0x008  4              15  ---  Number of Reported Uncorrectable Errors
0x04  0x010  4               6  ---  Resets Between Cmd Acceptance and Completion
0x06  =====  =               =  ===  == Transport Statistics (rev 1) ==
0x06  0x008  4              79  ---  Number of Hardware Resets
0x06  0x018  4               0  ---  Number of Interface CRC Errors
0x07  =====  =               =  ===  == Solid State Device Statistics (rev 1) ==
0x07  0x008  1              35  ---  Percentage Used Endurance Indicator
                                |||_ C monitored condition met
                                ||__ D supports DSN
                                |___ N normalized value

Pending Defects log (GP Log 0x0c) not supported

SATA Phy Event Counters (GP Log 0x11)
ID      Size     Value  Description
0x0001  4            0  Command failed due to ICRC error
0x000a  4            3  Device-to-host register FISes sent due to a COMRESET

