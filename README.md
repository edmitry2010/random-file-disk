# Random-file-disk

# ВНИМАНИЕ!!!
## Этот прокт создан исключительно для убивания дисков и для проверки стойкости дисков на нагрузку. Не используйте этот проект на своём основном ПК или боевых серверах, что бы избежать случайного повреждения ваших дисков.

## Если же вы всё таки решитесь убить свои диски, не расчитывайте на гарантию, т.к. нагрузка будет большая и ни один производителей не гаратирует работу своих дисков в таком режиме.

<p align="center"><img src="img/random-file-disk.jpg"/></p>

#### Обязательно укажите полный путь до файла per.ini во всех скриптах. Откройте каждый из них и укажите ваш полный путь.

Пример:
```
FilePer='/etc/myserver/scrypts/random-file-disk/per.ini'
```
```
gen-random.sh
start_count.sh
start_test.sh
```

#### Общий файл конфиг: per.ini
**Обязательно**, внимательно и точно укажите эти переменные.
```
DirConf='/etc/myserver/scrypts/random-file-disk'        # Рабочая папочка
...
DiskInLinux='/dev/sdb'                  # Диск, название указывается как в системе fdisk -l
DirMount='/mnt/test120'                 # Куда подмонтирован диск
TargetFolder="$DirMount/Random"         # Целевая папочка, которую подмонтировали для записи
```
Следующие переменные, можно оставить как есть, но в этом случае на текущем диске, в текущей папочке, скрипт создаст две папочки для рандомных файлов.
```
DirGenRandom1="$DirConf/GenRandom.1"    # Первая папочка с рандомными файлами
DirGenRandom2="$DirConf/GenRandom.2"    # Вторая папочка с рандомными файлами
```

Добавил возможность расчитывать переменные автоматический, после того как подмонтируете диск и укажите его в `per.ini`

Расчёт делается так.

Скрипт смотрит общий объём подключенного диска, отнимает от него указанный процент, переменная `PercentDrive` по умолчанию 20%.

Из полученного объёма, берёт 5% для размера одной папочки с рандомными файлами и 1% для указания максимального рандомного числа, которое потом используется в рандомайзере для указания размера файла.

Вот эти две переменные
```
RandomMax - Конечная цифра для генерации рандомного числа, 1% от рабочего объёма
MinSize - Размер папочек с рандомом, 5% от рабочего объёма
```
Для того, что бы скрипт сам определил параметры, укажите переменную `AutoPer` как `yes`

Если же вы сами хотите планировать размеры, укажите переменную `AutoPer` как `no` или закоментируйте. Но учтите, что заполнение целевого диска, одним циклом будет писать обе папочки, то есть первая + вторая (5+5=10Гб). Итоговый размер определяется переменной `CycleCount` и по умолчанию указанно `10`. В таком виде как сейчас указанны переменные, для 120-го диска, общий размер записи будет равняться примерно 100Гб.
```
CycleCount='10'         # Количество циклов, один цикл это запись двух папочек на целевой диск
```

Потом укажите переменные, на своё усмотрение:
```
RandomMax='100'         # Конечная цифра для генерации рандомного числа
MinSize='5120'          # 5 Гигабайт
```
Необходимые паузы.

Паузы требуются диску, однако если вы решили убить серверный диск, то паузы могут быть уменьшены до 1-5 секунд.

Вот пример:
```
PauseAfterCleaning='120'        # Пауза для освобождения SLC кэша в секундах, после очистки
PauseBetweenFolders='180'       # Пауза для освобождения SLC кэша в секундах между папочками
PauseBetweenCycles='180'        # Пауза для освобождения SLC кэша в секундах между циклами
PauseBeforeChecking='180'       # Пуаза для освобождения SLC кэша в секундах, перед проверкой md5sum
PauseAllDisk='600'              # Пауза для освобождения SLC кэша в секундах, перед записью в обратном порядке, весь диск
PauseBeforeStartingCycle='900'  # Пуаза для освобождения SLC кэша в секундах, перед запуском нового цикла
```
В целом, скрипты при автоматических расчётах, пригодны для любого размера дисков. Даже если поставите 2Tb диск, будет произведёт новый расчёт размеров и рандомных чисел и 20% от всего объёма диска выбрано не просто так. Дело в том, что файлы генерируются не точно и размер итоговых папочек может быть немного больше, потом они могут просто не влезть на целевой диск). 20% это нормально, заполнение будет почти полным, чего достаточно для теста.

#### Скрипт gen-random.sh
Предназначен для генерации рандомного размера с рандомными данными файлы, ни какой полезной информации эти файлы не содержат. Скрипт, создаёт файлы в указанных папочках до указанного минимального размера, с точностью до указанного размера в переменной `RandomMax`. Как только генератор достигнет нужного размера, скрипт создаст последний файл в текущей папочке. Папочка будет почти на один такой размер больше указанного размера `MinSize`, но не меньше.

Манипулируя переменными `RandomMin` и `RandomMax`, вы можете создавать файлы любого размера, которые гарантированно не будут содержать одинаковую информацию.

Что бы избежать перезаписи одной и той же информации на диск в разные циклы, здесь предусметренны две папочки, которые будут явно содержать разные файлы и примерно одинакового размера.

Переменные:
```
CountFor='1000'      # Общее количество генерируемых файлов, вероятно я уберу эту переменную в будущем.
```
Скрипт проверит наличие указанных в `per.ini` папочек для генерации файлов и если их нет создаст. Так же скрипт проверяет наличие уже сгенерированных рандомных файлов и если они есть, предложит удалить. Вы можете не удалять текущие сгенерированные файлы и продолжить, тогда скрипт просто добавит ещё файлов.

Ручная проверка размера папочек, при текущих настройках, пример:
```
du -hs /etc/myserver/scrypts/random-file-disk/GenRandom.1
du -hs /etc/myserver/scrypts/random-file-disk/GenRandom.2
```

#### Скрипт start_test.sh
Как раз этот скрипт и пишет/стирает данные на целевой диск, подготовленные ранее скриптом `gen-random.sh`. Полное описание работы, я уже почти сделал выше, включая картинку).

Перед каждым циклом записи, скрипт смотрит smart диска и если он не пройдён, запишет ошибку в лог `log/start_count.log` путь относительно проекта и прервёт выполнение скрипта.

Так же скрипт, сверяет записанные файлы, точнее md5sum с оригинальными и в случае не совпадения любого файла, так же запишет событие в лог `log/start_count.log` и прервёт выполнение скрипта.

Этот скрипт, дополнительно ведёт свою статистику по записанным данным и пишет их в `log/CountRecord_sdb.log`, итоговую цифру каждый раз обновляет в `log/CountRecord_sdb.disk`. Но из-за того, что bash не умеет считать дробные числа, я особо не старался вычислять точный объём и эта статистика очень приблизительно.

> Добавил бинарник **write_sum**, это простая программа написана на Go, она будет считать гигабайты и терабайты из смарт.
>
> Вот её код:
```
package main

import (
	"fmt"
	"os"
	"strconv"
)

func Gigabayte(a, b float64) float64 {
	return a * b / 1024 / 1024 / 1024
}

func Terabayte(a, b float64) float64 {
	return a * b / 1024 / 1024 / 1024 / 1024
}

func main() {
	if len(os.Args) < 3 {
		fmt.Println("Usage: go run main.go <num1> <num2>")
		return
	}

	x, err := strconv.ParseFloat(os.Args[1], 64)
	if err != nil {
		fmt.Println("Invalid argument:", os.Args[1])
		return
	}

	y, err := strconv.ParseFloat(os.Args[2], 64)
	if err != nil {
		fmt.Println("Invalid argument:", os.Args[2])
		return
	}

	resultG := Gigabayte(x, y)
	resultT := Terabayte(x, y)

	fmt.Println(resultG, "- Гигабайт")
	fmt.Println(resultT, "- Терабайт")
}
```
> Я пока не включал этот бинарник в код скриптов, но планирую это сделать.

#### Скрипт start_count.sh
