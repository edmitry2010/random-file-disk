#!/usr/bin/bash

FilePer='/etc/myserver/random-file-disk/per.ini'

if [ -f $FilePer ]; then
  . $FilePer
else
  echo "Неверно указан путь к файлу per.ini, выходим"
  exit 0
fi

SSDMount

ingen=($@)

CountFor='10000'	# Общее количество генерируемых файлов

TestDir $DirGenRandom1
TestDir $DirGenRandom2
TestDir $DirLog
TestDir $DirLogSmart
TestDir $DirLogStartCount

function TestEmptyDir() {
  local per=($*)
  ArrFind=(`find $per -depth -empty | wc -l`)

  if ! [[ ${ArrFind[0]} == 1 ]]; then
    read -r -p "Желаете очистить $per папочку (Y|yes|y), пропуск не очистит: " CleanDir
    case $CleanDir in
      Y|yes|y) rm -rf $per/*;;
    esac
  fi
}

case ${ingen[0]} in
  auto) PrintDisplay "$ViewYellow Чистим папочку:$ViewGreen $DirGenRandom1"
	rm -rf $DirGenRandom1/*
	PrintDisplay "$ViewYellow Чистим папочку:$ViewGreen $DirGenRandom2"
	rm -rf $DirGenRandom2/*
	echo;;
     *) TestEmptyDir $DirGenRandom1
        TestEmptyDir $DirGenRandom2;;
esac

function GenFilesDir() {
  local per=($*)
  local dir=${per[0]}
  local rnum=${per[1]}
  local tdate=${per[2]}
  local ttime=${per[3]}

#  echo "dd if=/dev/random of=$dir/random."$tdate"_"$ttime"  bs=1M  count=$rnum"
  dd if=/dev/random of=$dir/random."$tdate"_"$ttime"  bs=1M  count=$rnum 2> /dev/null
}

function StartRandom() {
  local per=($*)
  local dir=${per[0]}

  PrintDisplay "$ViewYellow Папочка в работе: $ViewGreen$dir"

  for ((i=1; i <= $CountFor; i++)); do
    Random
    TimeDate

    GenFilesDir $dir $RandomNum $Date $Time

    if [[ $i == 1 ]]; then
      CountNum=$( expr 0 + $RandomNum )
    else
      CountNum=$( expr $CountNum + $RandomNum )
    fi

    PrintDisplay "$ViewYellow Готово мегабайт: $ViewGreen$CountNum"
    if (( $CountNum >= $MinSize )); then
      PrintDisplay "$ViewYellow Достигнут минимальный предел размера папочки: $ViewGreen$MinSize$ViewYellow мегабайт"
      PrintDisplay "$ViewYellow Общий размер файлов: $ViewRed$CountNum$ViewYellow мегабайт"
      break
    fi
  done
  unset CountNum
}

StartRandom $DirGenRandom1
echo
StartRandom $DirGenRandom2

echo
PrintDisplay "$ViewYellow Пишем md5 суммы в файл"
HashMd5sum $DirGenRandom1 $FIleMd5sum
HashMd5sum $DirGenRandom2 $FIleMd5sum DOP
HashMd5sumClear $FIleMd5sum
HashMd5sumUniq $FIleMd5sum
