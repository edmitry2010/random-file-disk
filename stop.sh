#!/usr/bin/bash
# Этот скрипт, предназначен для остановки текущих скриптов, которые выполняются.
# Проще говоря, аварийная остановка.

Arr_pid=(`ps axuf | grep -v 'grep' | grep 'start_test.sh\|start_count.sh' | awk '{print $2}'`)

if ! [[ ${#Arr_pid[@]} == 0 ]]; then
  echo "Найдено ${#Arr_pid[@]} процесса: ${Arr_pid[@]}"
  for StopScrypt in ${Arr_pid[@]}; do
    echo "Останавливаем -> $StopScrypt"
    kill $StopScrypt
  done
else
  echo "Не найдено ни одного процесса"
fi
