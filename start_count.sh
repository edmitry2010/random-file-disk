#!/usr/bin/bash
# Этот скрипт, запускает start_test.sh указанное количество раз в фоновом режиме, логи будут в папочке log
# Аварийный выход, будет уже в start_test.sh, либо если Smart не пройдёт проверку, либо хэшь суммы файлов не сойдутся

# Запуск скрипта, следует выполнять так
# ./start_count.sh > /dev/null 2>&1 &

# Если выполнение скрипта прекращается по завершению вашей SSH сессии, можно использовать screen
# apt -y install screen
# screen -list - Посмотреть активные сессии
# screen -dr YOUR_PID - Подключиться к сессии
# screen - Новая сессия
# Ctrl+a+c - Будет создано новое окно, и вы перейдете к нему, вы можете запустить другие команды
# Ctrl+a+# - Где # от 0 до 9, вы перейдете в выбранное вами окно
# Ctrl+a+d - Отключите сеанс, и вы вернетесь к обычному терминалу
# screen -r - Повторно присоединяет вас к сеансу, если у вас их несколько, вас попросят ввести часть его имени
# (должна быть уникальная последовательность, содержащаяся в имени сеанса)

FilePer='/etc/myserver/random-file-disk/per.ini'

if [ -f $FilePer ]; then
  . $FilePer
else
  echo "Неверно указан путь к файлу per.ini, выходим"
  exit 0
fi

SSDMount

TestDir $DirLogStartCount

TimeDate
DateStart=`date`

CountStartTest='40'	# Количество стартов

# Очистка логов перед новым стартом
#truncate -s 0 $DirLogStartCount/start_count.$Date.$Time.log
truncate -s 0 $DirLog/error.log

echo "######## Запуск $DateStart ########" >> $DirLogStartCount/start_count.$Date.$Time.log

echo "Сообщение от $thost" > $msg
# SmartRecordedData
# PrintDisplay "$ViewYellow Итого записано на диск гигабайт:$ViewGreen $RecordGBdata" >> $msg
# PrintDisplay "$ViewYellow Итого записано на диск терабайт:$ViewGreen $RecordTBdata" >> $msg

for ((i=1; i <= $CountStartTest; i++)); do
  DateCycle=`date`
  echo "######## Запуск цикла №/$i $DateCycle ########" >> $DirLogStartCount/start_count.$Date.$Time.Start.$i.log
  echo "Запуск №/$i $DateCycle" >> $msg
  # curl $sendmsg -d chat_id=$chat_id -d text="$(<$msg)" > /dev/null 2>&1 &
  SendTGmsg

  $DirConf/start_test.sh 1>> $DirLogStartCount/start_count.$Date.$Time.Start.$i.log

  . $DirLog/error.log

  if [[ $SmartError == 'yes' || $HashError == 'yes' ]]; then
    echo $SmartError >> $DirLogStartCount/start_count.$Date.$Time.Start.$i.log
    echo $HashError >> $DirLogStartCount/start_count.$Date.$Time.Start.$i.log

    echo "Сообщение от $thost" > $msg
    echo "######## Имеются ошибки, выходим `date`" >> $DirLogStartCount/start_count.$Date.$Time.Start.$i.log
    echo "Имеются ошибки, выходим `date`" >> $msg
    curl $sendmsg -d chat_id=$chat_id -d text="$(<$msg)" > /dev/null 2>&1 &

    unset SmartError
    unset HashError
    exit 0
  fi

  if [[ $i != $CountStartTest ]]; then
    echo
    PrintDisplay "$ViewYellow Пауза для освобождения SLC кэша в секундах, перед запуском нового цикла: $ViewGreen$PauseBeforeStartingCycle"
    PrintDisplay "$ViewYellow Текущее время: $ViewGreen`date`"
    sleep $PauseBeforeStartingCycle
  fi
done
unset i

DateStop=`date`
echo "######## Запуск был $DateStart ########" >> $DirLogStartCount/start_count.$Date.$Time.log
echo "######## Остановка $DateStop ########" >> $DirLogStartCount/start_count.$Date.$Time.log

echo "Сообщение от $thost" > $msg
echo "Запуск был $DateStart" >> $msg
echo "Остановка $DateStop" >> $msg
# curl $sendmsg -d chat_id=$chat_id -d text="$(<$msg)" > /dev/null 2>&1 &
SendTGmsg

unset DateStart
unset DateStop
unset CountStartTest
