#!/usr/bin/bash

FilePer='/etc/myserver/random-file-disk/per.ini'

if [ -f $FilePer ]; then
  . $FilePer
else
  echo "Неверно указан путь к файлу per.ini, выходим"
  exit 0
fi

instarttest=($@)

SSDMount

SmartRecordedData
PrintDisplay "$ViewYellow Итого записано на диск гигабайт:$ViewGreen $RecordGBdata"
PrintDisplay "$ViewYellow Итого записано на диск терабайт:$ViewGreen $RecordTBdata"
echo
PrintDisplay "$ViewYellow Генерируем новые рандомные файлы"

$DirConf/gen-random.sh auto
TestCleanDir
TrimDisk $DirMount

###### Начали, первая запись ######
PrintDisplay "$ViewYellow Начали, первая запись"
FillingDisk $DirGenRandom1 $DirGenRandom2
echo
PrintDisplay "$ViewYellow Пауза для освобождения SLC кэша в секундах, перед trim: $ViewGreen$PauseBeforeChecking"
PrintDisplay "$ViewYellow Текущее время: $ViewGreen`date`"

sleep $PauseBeforeChecking
echo
TrimDisk $DirMount
echo
PrintDisplay "$ViewYellow Пауза, убедимся в занятом размере и дописываем остатки диска: $ViewGreen$PauseBeforeChecking"
sleep $PauseBeforeChecking
AddingFiles
TrimDisk $DirMount

PrintDisplay "$ViewYellow Смотрим md5sum записанных файлов"

HashMd5sum $TargetFolder $FIleMd5sumTarget
HashMd5sumClear $FIleMd5sumTarget
HashMd5sumUniq $FIleMd5sumTarget

DiffLogs=(`diff $FIleMd5sum $FIleMd5sumTarget | wc -l`)

PrintDisplay "$ViewYellow На диске, осталось не записано:$ViewGreen `df -h $DirMount | grep $DirMount | awk '{print $4}'`"

if [[ ${DiffLogs[0]} == 0 ]]; then
  PrintDisplay "$ViewYellow Хэш суммы файлов md5sum впорядке, продолжаем"
else
  PrintDisplay "$ViewRed Хэш суммы файлов md5sum не впорядке, выходим"
  echo "Сообщение от $thost" > $msg
  echo "######## Аварийный выход по хэш сумме `date` ########" >> $DirLogStartCount/start_count.$Date.$Time.log
  echo HashError='yes' >> $DirLog/error.log
  echo "Аварийный выход по хэш сумме `date`" >> $msg

  SmartRecordedData
  PrintDisplay "$ViewYellow Итого записано на диск гигабайт:$ViewGreen $RecordGBdata" >> $msg
  PrintDisplay "$ViewYellow Итого записано на диск терабайт:$ViewGreen $RecordTBdata" >> $msg

  curl $sendmsg -d chat_id=$chat_id -d text="$(<$msg)" > /dev/null 2>&1 &
  exit 0
fi

echo
PrintDisplay "$ViewYellow Пауза для освобождения SLC кэша в секундах, перед записью в обратном порядке, весь диск: $ViewGreen$PauseAllDisk"
PrintDisplay "$ViewYellow Текущее время: $ViewGreen`date`"
sleep $PauseAllDisk
echo

TestCleanDir
TrimDisk $DirMount

###### Начали, вторая запись ######
PrintDisplay "$ViewYellow Начали, первая запись"
FillingDisk $DirGenRandom2 $DirGenRandom1

echo
PrintDisplay "$ViewYellow Пауза для освобождения SLC кэша в секундах, перед trim: $ViewGreen$PauseBeforeChecking"
PrintDisplay "$ViewYellow Текущее время: $ViewGreen`date`"

sleep $PauseBeforeChecking
echo
TrimDisk $DirMount
echo
PrintDisplay "$ViewYellow Пауза, убедимся в занятом размере и дописываем остатки диска: $ViewGreen$PauseBeforeChecking"
sleep $PauseBeforeChecking
AddingFiles
TrimDisk $DirMount

PrintDisplay "$ViewYellow Смотрим md5sum записанных файлов"

HashMd5sum $TargetFolder $FIleMd5sumTarget
HashMd5sumClear $FIleMd5sumTarget
HashMd5sumUniq $FIleMd5sumTarget

SmartRecordedData
PrintDisplay "$ViewYellow Итого записано на диск гигабайт:$ViewGreen $RecordGBdata"
PrintDisplay "$ViewYellow Итого записано на диск терабайт:$ViewGreen $RecordTBdata"

DiffLogs=(`diff $FIleMd5sum $FIleMd5sumTarget | wc -l`)

TrimDisk $DirMount

PrintDisplay "$ViewYellow Осталось не записано:$ViewGreen `df -h $DirMount | grep $DirMount | awk '{print $4}'`"

if [[ ${DiffLogs[0]} == 0 ]]; then
  PrintDisplay "$ViewYellow Хэш суммы файлов md5sum впорядке, завершаем цикл"
else
  PrintDisplay "$ViewRed Хэш суммы файлов md5sum не впорядке, выходим"
  echo "Сообщение от $thost" > $msg
  echo "######## Аварийный выход по хэш сумме `date` ########" >> $DirLogStartCount/start_count.$Date.$Time.log
  echo HashError='yes' >> $DirLog/error.log
  echo "Аварийный выход по хэш сумме `date`" >> $msg

  SmartRecordedData
  PrintDisplay "$ViewYellow Итого записано на диск гигабайт:$ViewGreen $RecordGBdata" >> $msg
  PrintDisplay "$ViewYellow Итого записано на диск терабайт:$ViewGreen $RecordTBdata" >> $msg

  curl $sendmsg -d chat_id=$chat_id -d text="$(<$msg)" > /dev/null 2>&1 &
  exit 0
fi

###### Очистка целевой папочки ######
if [ -d $TargetFolder ]; then
  ArrDirEmpty=(`find $TargetFolder -depth -empty | wc -l`)
  if ! [[ ${ArrDirEmpty[0]} == 1 ]]; then
    PrintDisplay "$ViewYellow Очистка целевой папочки: $ViewGreen$TargetFolder"
    rm -rf $TargetFolder/*
  fi
fi
unset ArrDirEmpty
